package springBoot.XOR;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import springBoot.exceptions.BadRequestException;
import springBoot.utils.Type;

import java.util.HashMap;
import java.util.Map;

@Controller
public class XORController {


    public static final Map<Character, String> encoding = new HashMap<>();
    public static final Map<String, Character> decoding = new HashMap<>();
    // !#$%&()*+,-.а0б1в2г3д4е5ж6з7и8й9к:лм<н=о>п?ртуфхцчшщъыьэюяё^_c

    public String gamm;

    public static void initAlph() {
        encoding.put('й', "000000");
        encoding.put('ц', "000001");
        encoding.put('у', "000010");
        encoding.put('к', "000011");
        encoding.put('е', "000100");
        encoding.put('н', "000101");
        encoding.put('г', "000110");
        encoding.put('ш', "000111");
        encoding.put('щ', "001000");
        encoding.put('з', "001001");
        encoding.put('х', "001010");
        encoding.put('ъ', "001011");
        encoding.put('ф', "001100");
        encoding.put('ы', "001101");
        encoding.put('в', "001110");
        encoding.put('а', "001111");
        encoding.put('п', "010000");
        encoding.put('р', "010001");
        encoding.put('о', "010010");
        encoding.put('л', "010011");
        encoding.put('д', "010100");
        encoding.put('ж', "010101");
        encoding.put('э', "010110");
        encoding.put('я', "010111");
        encoding.put('ч', "011000");
        encoding.put('м', "011001");
        encoding.put('и', "011010");
        encoding.put('т', "011011");
        encoding.put('ь', "011100");
        encoding.put('б', "011101");
        encoding.put('ю', "011110");
        encoding.put('1', "011111");
        encoding.put('2', "100000");
        encoding.put('3', "100001");
        encoding.put('4', "100010");
        encoding.put('5', "100011");
        encoding.put('6', "100100");
        encoding.put('7', "100101");
        encoding.put('8', "100110");
        encoding.put('9', "100111");
        encoding.put('0', "101000");
        encoding.put(',', "101001");
        encoding.put('.', "101010");
        encoding.put('!', "101011");
        encoding.put('?', "101100");
        encoding.put('-', "101101");
        encoding.put(':', "101110");
        encoding.put('ё', "101111");
        encoding.put('\n', "110000");
        encoding.put(' ', "110001");
        encoding.put('с', "110010");
        encoding.put('#', "110011");
        encoding.put('$', "110100");
        encoding.put('%', "110101");
        encoding.put('^', "110110");
        encoding.put('&', "110111");
        encoding.put('*', "111000");
        encoding.put('(', "111001");
        encoding.put(')', "111010");
        encoding.put('_', "111011");
        encoding.put('+', "111100");
        encoding.put('=', "111101");
        encoding.put('>', "111110");
        encoding.put('<', "111111");

        encoding.forEach((key, value) -> decoding.put(value, key));
    }

    private char XOR(char a, char b) {
        int first = Integer.parseInt(String.valueOf(a));
        int second = Integer.parseInt(String.valueOf(b));

        int result = (first + second) % 2;

        return String.valueOf(result).charAt(0);
    }

    private void keyGen(String message, long n) {
        gamm = "";
        for (int i = 0; i < message.length(); i++) {
            gamm += encoding.get(message.charAt(i));
        }
        System.out.println("Key:             " + gamm);
        int k = 0;
        while (gamm.length() < n * 6) {
            gamm += XOR(gamm.charAt(k), gamm.charAt(gamm.length() - 1));
            k++;
        }
        System.out.println("Extended key:    " + gamm);
    }

    private String validateKey(String key) throws BadRequestException {
        String res = "";
        for (int i = 0; i < key.length(); i++) {
            if (encoding.containsKey(Character.toLowerCase(key.charAt(i)))) {
                res += Character.toLowerCase(key.charAt(i));
            }
        }
        return res;
    }

    private String validateMessage(String message) {
        String result = "";
        for (int i = 0; i < message.length(); i++) {
            if (encoding.containsKey(Character.toLowerCase(message.charAt(i)))) {
                result += Character.toLowerCase(message.charAt(i));
            } else {
                System.out.println("Symbol miss: " + message.charAt(i));
            }
        }
        return result;
    }

    private String xxcrypt(String message) {
        String buf = "";
        String result = "";
        for (int i = 0; i < message.length(); i++) {
            buf += encoding.get(message.charAt(i));
        }
        System.out.println("Message:         " + buf);
        String toDecode = "";
        System.out.print("Crypted message: ");
        while (!buf.isEmpty()) {
            toDecode += XOR(buf.charAt(0), gamm.charAt(0));
            buf = buf.substring(1);
            gamm = gamm.substring(1);
            if (toDecode.length() == 6) {
                System.out.print(toDecode);
                result += decoding.get(toDecode);
                toDecode = "";
            }
        }
        System.out.println();
        return result;
    }

    @GetMapping("/XOR")
    public String indexXOR() {
        return "Gamm.html";
    }

    @PostMapping("/XOR")
    @ResponseBody
    public ResponseEntity<String> process(@RequestBody String JSONmessage) {
        if (encoding.isEmpty()) {
            initAlph();
        }

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> message = null;
            try {
                message = objectMapper.readValue(JSONmessage, Map.class);
            } catch (JsonProcessingException e) {
                return new ResponseEntity<String>("Error while reading message", HttpStatus.I_AM_A_TEAPOT);
            }

            String text = (String) message.get("message");
            String key = (String) message.get("key");

            text = validateMessage(text);
            key = validateKey(key);
            keyGen(key, text.length());

            return new ResponseEntity<>(xxcrypt(text), HttpStatus.OK);

        } catch (Exception e) {
            if (e instanceof BadRequestException) {
                return new ResponseEntity<String>(e.getMessage(), HttpStatus.I_AM_A_TEAPOT);
            }
        }
        return null;
    }

}
