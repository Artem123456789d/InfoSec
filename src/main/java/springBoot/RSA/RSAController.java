package springBoot.RSA;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import springBoot.exceptions.BadRequestException;
import springBoot.utils.Pair;
import springBoot.utils.Type;

import java.math.BigInteger;
import java.util.*;


@Controller
public class RSAController {
    private Pair<Long, Long> secretKey;
    private Pair<Long, Long> publicKey;
    private Pair<Long, Long> prevSecretKey;


    public static final Map<Character, Integer> encoding = new HashMap<>();
    public static final Map<Integer, Character> decoding = new HashMap<>();
    public static int biggestEnc;


    public static void initAlph() {
        encoding.put('й', 11);
        encoding.put('ц', 12);
        encoding.put('у', 13);
        encoding.put('к', 14);
        encoding.put('е', 15);
        encoding.put('н', 16);
        encoding.put('г', 17);
        encoding.put('ш', 18);
        encoding.put('щ', 19);
        encoding.put('з', 21);
        encoding.put('х', 22);
        encoding.put('ъ', 23);
        encoding.put('ф', 24);
        encoding.put('ы', 25);
        encoding.put('в', 26);
        encoding.put('а', 27);
        encoding.put('п', 28);
        encoding.put('р', 29);
        encoding.put('о', 31);
        encoding.put('л', 32);
        encoding.put('д', 33);
        encoding.put('ж', 34);
        encoding.put('э', 35);
        encoding.put('я', 36);
        encoding.put('ч', 37);
        encoding.put('м', 38);
        encoding.put('и', 39);
        encoding.put('т', 41);
        encoding.put('ь', 42);
        encoding.put('б', 43);
        encoding.put('ю', 44);
        encoding.put('1', 45);
        encoding.put('2', 46);
        encoding.put('3', 47);
        encoding.put('4', 48);
        encoding.put('5', 49);
        encoding.put('6', 51);
        encoding.put('7', 52);
        encoding.put('8', 53);
        encoding.put('9', 54);
        encoding.put('0', 55);
        encoding.put(',', 56);
        encoding.put('.', 57);
        encoding.put('!', 58);
        encoding.put('?', 59);
        encoding.put('-', 61);
        encoding.put(':', 62);
        encoding.put('ё', 63);
        encoding.put('\n', 64);
        encoding.put(' ', 65);
        encoding.put('с', 66);

        biggestEnc = 66;
        encoding.forEach((key, value) -> decoding.put(value, key));
    }

    private boolean isPrime(long number) {
        long n = Math.round(Math.sqrt(number)) + 1;
        for (long i = 2; i < n; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    private long gcd(long num1, long num2) {
        long gcd = 1;
        for (long i = 1; i <= num1 && i <= num2; i++) {
            if (num1 % i == 0 && num2 % i == 0) {
                gcd = i;
            }
        }
        return gcd;
    }

    private boolean soloveyStrassen(long n) throws BadRequestException {
        Random random = new Random();
        for (int i = 1; i < 1000000; i++) {
            long a = 0;
            int tryNumber = 0;
            while (a < 2 || a > n - 1) {
                if (tryNumber > 10000) {
                    throw new BadRequestException("Невозможно определить простоту числа " + n);
                }
                a = (long) Math.floor(random.nextDouble() * n);
                tryNumber++;
            }
            if (gcd(a, n) > 1) {
                return false;
            }
        }
        return true;
    }

    private void keyGen(long p, long q) throws BadRequestException {
        long n = p * q;
        long euler = (p - 1) * (q - 1);
        long d = -1;
        Random random = new Random();

        long start = Math.round(random.nextFloat() * n);
        for (long i = start; i < 9223372036854775783L; i++) {
            if (gcd(i, euler) == 1) {
                d = i;
                break;
            }
        }
        if (d == -1) {
            throw new BadRequestException("Невозможно создать секретный ключ");
        }

        long e = -1;
        start = Math.round(random.nextFloat() * n);
        for (long i = start; i < 9223372036854775783L; i++) {
            if ((i * d) % euler == 1) {
                e = i;
                break;
            }
        }
        if (e == -1) {
            throw new BadRequestException("Невозможно создать публичный ключ");
        }

        publicKey = new Pair<>(e, n);
        secretKey = new Pair<>(d, n);
    }


    private void generateRandomKeys() throws BadRequestException {
        //random key generation
        long start = 200;
        Random random = new Random();
        long p = Math.round(random.nextFloat() * start);
        while (!soloveyStrassen(p)) {
            p++;
        }
        long q = Math.round(random.nextFloat() * start);
        while (!soloveyStrassen(q)) {
            q++;
        }

        keyGen(p, q);
    }

    private List<Long> messageToNumbers(String message) {
        //message encoding
        List<Long> result = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            char curChar = Character.toLowerCase(message.charAt(i));
            if (encoding.containsKey(curChar)) {
                //if encoding has not symbol
                builder.append(encoding.get(curChar));
            } else {
                System.out.println("Symbol miss " + curChar);
            }
        }
        String buf = builder.toString();

        int numberOfDigits = String.valueOf(Math.abs(publicKey.getSecond())).length();

        while (!buf.isEmpty()) {
            int k = Math.min(buf.length(), numberOfDigits);
            if (Long.parseLong(buf.substring(0, k)) > publicKey.getSecond()) {
                result.add(Long.parseLong(buf.substring(0, k - 1)));
                buf = buf.substring(k - 1);
            } else {
                result.add(Long.parseLong(buf.substring(0, k)));
                buf = buf.substring(k);
            }

        }
        return result;
    }

    private String numbersToMessage(List<Long> numbers) {
        String result = "";
        StringBuilder builder = new StringBuilder();

        for (Long number : numbers) {
            builder.append(number);
        }

        //message decoding
        String buf = builder.toString();

        while (!buf.isEmpty()) {
            int k = Math.min(2, buf.length());
            int code = Integer.parseInt(buf.substring(0, k));
            if (code > biggestEnc) {
                if (buf.length() % 2 == 0) {
                    result += "#";
                    buf = buf.substring(k);
                } else {
                    code = Integer.parseInt(buf.substring(0, k - 1));
                    result += "#";
                    buf = buf.substring(k - 1);
                }
            } else {
                if (decoding.containsKey(code)) {
                    result += decoding.get(code);
                } else {
                    result += "#";
                }
                buf = buf.substring(k);
            }
        }
        return result;
    }

    private List<Long> encrypt(List<Long> numbers) {
        List<Long> res = new ArrayList<>();

        //m^e mod n
        numbers.forEach(num -> {
            BigInteger pow = new BigInteger(String.valueOf(num));
            int exp = (int) ((long) publicKey.getFirst());
            pow = pow.pow(exp);
            BigInteger mod = pow.mod(new BigInteger(String.valueOf(publicKey.getSecond())));
            res.add(mod.longValue());
        });
        return res;
    }

    private List<Long> decrypt(List<Long> numbers) {
        List<Long> res = new ArrayList<>();

        //c^d mod n
        numbers.forEach(num -> {
            BigInteger pow = new BigInteger(String.valueOf(num));
            int exp = (int) ((long) secretKey.getFirst());
            pow = pow.pow(exp);
            res.add((pow.mod(new BigInteger(String.valueOf(secretKey.getSecond())))).longValue());
        });
        return res;
    }

    private String encryption(String message) {
        List<Long> openBlocks = messageToNumbers(message);
        System.out.println("public key: " + publicKey);
        System.out.println("secret key: " + secretKey);
        System.out.println("Open blocks: " + openBlocks);
        List<Long> cryptoBlocks = encrypt(openBlocks);
        System.out.println("Crypto blocks: " + cryptoBlocks);
        StringBuilder builder = new StringBuilder();

        cryptoBlocks.forEach(block -> builder.append(block).append(','));

        String res = builder.toString().substring(0, builder.length() - 1);
        return res;
    }

    private String decryption(String message) {
        String[] convertedMessageArray = message.split(","); //parsing cryptogram from csv format
        List<Long> convertedMessageList = new ArrayList<>();
        for (String number : convertedMessageArray) {
            convertedMessageList.add(Long.parseLong(number.trim()));
        }

        System.out.println("public key: " + publicKey);
        System.out.println("secret key: " + secretKey);
        System.out.println("Received blocks: " + convertedMessageList);
        List<Long> decryptBlocks = decrypt(convertedMessageList); //decryption
        System.out.println("Decrypted blocks: " + decryptBlocks);
        String res = numbersToMessage(decryptBlocks); //decoding
        System.out.println(res);
        return res;
    }


    @GetMapping(value = "/RSA")
    public String getIndexRSA() {
        return "RSA.html";
    }

    @PostMapping(value = "/RSA")
    @ResponseBody
    public ResponseEntity<String> processRequest(@RequestBody String JSONmessage) {
        if (encoding.isEmpty()) {
            initAlph();
        }
        try {
            //Message parsing
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> message = null;
            try {
                message = objectMapper.readValue(JSONmessage, Map.class);
            } catch (JsonProcessingException e) {
                return new ResponseEntity<String>("Error while reading message", HttpStatus.I_AM_A_TEAPOT);
            }

            String text = (String) message.get("message");

            Type type = Type.valueOf((String) message.get("type"));
            boolean generateKeys = (boolean) message.get("generate");
            String nString = (String) message.get("n");
            String dString = (String) message.get("d");

            //If user wants to Encrypt message
            if (type == Type.ENCRYPT) {
                if (generateKeys) { //if user wants to generate new Keys with p and q
                    int p = Integer.parseInt((String) message.get("p"));
                    int q = Integer.parseInt((String) message.get("q"));
                    if (p == q) {
                        return new ResponseEntity<>("P и Q должны быть различными", HttpStatus.I_AM_A_TEAPOT);
                    }
                    if (!soloveyStrassen(p) || !soloveyStrassen(q)) { //prime - check
                        return new ResponseEntity<String>("P и Q должны быть простыми", HttpStatus.I_AM_A_TEAPOT);
                    } else {
                        do {
                            keyGen(p, q);
                        } while (secretKey.getFirst().equals(publicKey.getFirst())); //generating keys while e and d are not different
                    }
                }
                if (publicKey == null || secretKey == null) { //if user doesn't wants to generate new keys and server hasn't keys in memory
                    do {                                    //generate random keys while  e and d are not different
                        generateRandomKeys();
                    } while (secretKey.getFirst().equals(publicKey.getFirst()));
                }
                String result = encryption(text); //encryption of open text
                return new ResponseEntity<String>(result, HttpStatus.OK); //returning result to user
            } else { //If user wants to Decrypt message
                if (nString != null && dString != null) { //if user wants to decrypt with his own keys
                    long n = Long.parseLong(nString);
                    long d = Long.parseLong(dString);
                    prevSecretKey = secretKey;
                    secretKey = new Pair<>(d, n);
                }
                String result = decryption(text); //decryption of cryptogramm
                if (prevSecretKey != null) {
                    secretKey = prevSecretKey;
                }
                return new ResponseEntity<String>(result, HttpStatus.OK); //returning result to user
            }
        } catch (Exception e) {
            if (e instanceof BadRequestException) {
                return new ResponseEntity<String>(e.getMessage(), HttpStatus.I_AM_A_TEAPOT);
            } else if (e instanceof NumberFormatException) {
                return new ResponseEntity<>("Неозможно обработать криптограмму. Проверьте ввод", HttpStatus.I_AM_A_TEAPOT);
            }
        }
        return null;
    }

    @GetMapping("/RSA/KEYS")
    @ResponseBody
    public String getKeys() {
        if (secretKey == null || publicKey == null) {
            return "No keys";
        }
        return "Private key: " + secretKey.getFirst() + ", " + secretKey.getSecond() + "\n"
                + "Public key: " + publicKey.getFirst() + ", " + publicKey.getSecond();
    }

    @GetMapping("/RSA/RESET")
    public void reset() {
        secretKey = null;
        publicKey = null;
        prevSecretKey = null;
    }
}