let hasError = false;

function send() {
    let xhr = new XMLHttpRequest();
    let msg = document.getElementById("inputMessage").value;
    let key = document.getElementById("key").value;
    // let type = document.getElementById("type").value;

    let message = {
        // type: type,
        key: key || null,
        message: msg
    }

    xhr.open("POST", '/XOR', true);
    xhr.setRequestHeader("Content-type", "application/json");

    xhr.onreadystatechange = function () {
        if (xhr.status === 200) {
            document.getElementById("outputMessage").value = xhr.responseText;
        } else if (xhr.status === 418 && !hasError && xhr.responseText !== "") {
            alert(xhr.responseText);
            hasError = true;
        }
    }

    hasError = false;
    xhr.send(JSON.stringify(message));

}