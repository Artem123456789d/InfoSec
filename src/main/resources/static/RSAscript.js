function showPQ() {
    let checkBox = document.getElementById("genNewKeys");
    let text = document.getElementById("hiddenDiv");
    if (checkBox.checked === true) {
        text.style.display = "block";
    } else {
        text.style.display = "none";
    }
}

let hasError = false;
let notified = false;

function send() {
    let xhr = new XMLHttpRequest();
    let p = document.getElementById("inputP").value;
    let q = document.getElementById("inputQ").value;
    let generate = document.getElementById("genNewKeys").checked;
    let msg = document.getElementById("inputMessage").value;
    let d = document.getElementById("inputD").value;
    let n = document.getElementById("inputN").value;
    let type = document.getElementById("type").value;
    debugger
    if (type === "DECRYPT" && (d !== "" || n !== "") && !notified) {
        alert("В случае неверных данных в ключе, сервер не сможет верно расшифровать Ваше сообщение.");
        notified = true;
    }

    let message = {
        type: type,
        p: p || null,
        q: q || null,
        generate: generate,
        message: msg,
        n: n || null,
        d: d || null
    }

    xhr.open("POST", '/RSA', true);
    xhr.setRequestHeader("Content-type", "application/json");

    xhr.onreadystatechange = function () {
        if (xhr.status === 200) {
            document.getElementById("outputMessage").value = xhr.responseText;
        } else if (xhr.status === 418 && !hasError && xhr.responseText !== "") {
            alert(xhr.responseText);
            hasError = true;
        }
    }

    hasError = false;
    xhr.send(JSON.stringify(message));

}

function getKeys() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", '/RSA/KEYS', true);

    xhr.onreadystatechange = function () {
        if (xhr.status === 200) {
            document.getElementById("outputMessage").value = xhr.responseText;
        } else if (xhr.status === 418) {
            alert("Error during processing request. Please, check your input.");
        }
    }

    xhr.send();

}

function showKey() {
    let type = document.getElementById("type");
    let text = document.getElementById("secretKey");
    if (type.value === "DECRYPT") {
        document.getElementById("genNewKeys").checked = false;
        document.getElementById("genNewKeys").disabled = true;
        showPQ();
        text.style.display = "block";
    } else {
        document.getElementById("genNewKeys").disabled = false;
        text.style.display = "none";
    }
}